﻿/**
 * Saving all the settings made in the options menu. Can be written to a json file for permanent storage.
 **/

public class GameSettings {
    public bool fullscreen;
    public bool tutorial;
    public int language;
    public int textureQuality;
    public int antialiasing;
    public int resolutionIndex;
    public int mirrors;
}
